// Fonta para fazer input: https://www.codecademy.com/article/getting-user-input-in-node-js
// caso queira usar o prompt tem que apagar o arquivo "package.json"

// 2. Escreva um algoritmo para ler uma temperatura em graus Fahrenheit,
// calcular e escrever o valor correspondente em graus Celsius (baseado na
// fórmula abaixo):
// 𝐶/5 = ( 𝑓 − 32)/9

// vou receber em f -> c
// c = f-32 -> /9 -> *5
const prompt = require('prompt-sync')();

let temperatua = prompt("Valor temperatura em graus Fahrenheit: ");
temperatua = parseInt(temperatua)

let temp_celcius = (((temperatua - 32)/ 9)* 5)
console.log(temp_celcius)