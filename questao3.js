// caso queira usar o prompt tem que apagar o arquivo "package.json"
const prompt = require('prompt-sync')();

// Faça um algoritmo em que você recebe 3 notas de um aluno e caso a
// média aritmética dessas notas for maior ou igual que 6 imprima
// “Aprovado”, caso contrário “Reprovado

let nota1 = prompt("Digite a primeira nota: "), nota2 = prompt("Digite a segunda nota: "), nota3 = prompt("Digite a terceira nota: ");
nota1 = parseInt(nota1);
nota2 = parseInt(nota2);
nota3 = parseInt(nota3);

let final = ((nota1+nota2+nota3)/3);
final = final.toFixed(2)
if (final < 6) 
{
    console.log("Reprovado");    
}
else {
    console.log("Aprovado");
}
