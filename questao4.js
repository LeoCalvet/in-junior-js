// 4. Vocês receberão um arquivo com um array de objetos representando
// deuses do jogo Smite. Usando os métodos aprendidos em aula, faça os
// seguintes exercícios:

// ● Q1. Imprima o nome e a quantidade de features de todos os deuses usando
// uma única linha de código.

// ● Q2. Imprima todos os deuses que possuem o papel de "Mid"

// ● Q3. Organize a lista pelo panteão do deus.

// ● Q4. Faça um código que retorne um novo array com o nome de cada deus e
// entre parênteses, a sua classe.

// Por exemplo, o array deverá ficar assim: ["Achilles (Warrior)", "Agni (Mage)", ...]
import gods from './arquivo_exercicio_4.js'

//Q1. Imprima o nome e a quantidade de features de todos os deuses usando uma única linha de código.
gods.forEach(god => console.log(god.name, god.features.length));


//Q2. Imprima todos os deuses que possuem o papel de "Mid"
for (let i = 0; i < gods.length; i++) 
{
    if (gods[i].roles.includes("Mid")) 
    {
        console.log(gods[i]);
    }
}

//Q3. Organize a lista pelo panteão do deus
gods.sort((deus1, deus2) => {
    return (deus1.pantheon > deus2.pantheon) ? 1 : ((deus2.pantheon > deus1.pantheon) ? -1 : 0);
});
console.log(gods);

//Q4. Faça um código que retorne um novo array com o nome de cada deus e entre parênteses, a sua classe
let saida = [];
for (let deus of gods)
{
    saida.push(deus.name+" ("+deus.class+")")
}
for (let pos of saida)
{
    console.log(pos)
}